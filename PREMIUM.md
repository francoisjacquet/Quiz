Quiz Premium module
===================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot.png)

FEATURES
--------

- Copy Quiz from Last Year
- Search Questions
- Answer Breakdown:
  - Identify questions that students have difficulties answering
- Configuration:
  - Teachers can only edit their own Questions and Administrators or other Teachers can only view them

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_3.png)

### [➭ Switch to Premium](https://www.rosariosis.org/modules/quiz/#premium-module)
