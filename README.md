Quiz module
===========

![screenshot](https://gitlab.com/francoisjacquet/Quiz/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/modules/quiz/

Version 11.1 - March, 2025

License GNU/GPLv2 or later

Author François Jacquet

DESCRIPTION
-----------
Quiz module for RosarioSIS. Teachers can add questions and answers to the questions database, and then add them their quizzes.
Quizzes are open for submission based on the corresponding Assignment's assigned and due dates. Teachers can decide whether to show correct answers to students and display questions in random order.
Students take their tests / quizzes online. Select, multiple, gap fill and text question types can be graded automatically.
Inline help.

Translated in [French](https://www.rosariosis.org/fr/modules/quiz/), [Spanish](https://www.rosariosis.org/es/modules/quiz/), Slovenian and Portuguese (Brazil).

Premium module:
- Copy Quiz from Last Year
- Search Questions
- Answer Breakdown:
  - Identify questions that students have difficulties answering
- Configuration:
  - Teachers can only edit their own Questions and Administrators or other Teachers can only view them

Note: both the Quiz and Quiz Premium modules must be activated for the Premium module to work.


CONTENT
-------
Quiz
- Quizzes
- Questions

INSTALL
-------
Copy the `Quiz/` folder (if named `Quiz-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 6.8+
