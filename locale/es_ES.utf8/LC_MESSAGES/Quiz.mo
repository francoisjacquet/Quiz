��    "      ,  /   <      �  E   �     ?     K     X     _     p     y     �     �  '   �     �  
   �  '   �               3  =   <     z     �     �  	   �     �     �     �  @   �          /     G  %   \     �  ,   �  +   �  "   �      T        k     �  	   �     �  
   �  
   �     �     �  /   	     1	     C	  3   W	     �	     �	     �	  @   �	     
     
     '
  	   ?
     I
     V
     h
  O   p
     �
     �
      �
  (        D  ,   V  0   �  "   �                                      "      !             	                                                                 
                                    *Correct option #1
Wrong option #2
Wrong option #3
*Correct option #4 *True
False Add Question Answer Answer Breakdown Answered Answers Back to Quizzes Correct answer (optional). Delimit gaps with double underscores __ Gap Fill Grade Quiz Mark correct answer with an asterisk *. New Question New Question Category New Quiz No Assignments are available. Please add a new Assignment: %s Question Question Categories Question Category Questions Quiz Premium Quiz submitted. Quizzes Quizzes already contain this question. Please add a new Quiz: %s Random Question Order Select One from Options Show Correct Answers Submissions for this quiz are closed. Text with gaps The sky is __blue__.
The grass is __green__. This program is available in the %s module. You already have graded this Quiz. Project-Id-Version: Quiz module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-07 19:02+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 *Opción correcta #1
Opción incorrecta #2
Opción incorrecta#3
*Opción correcta #4 *Correcto
Incorrecto Agregar Pregunta Respuesta Análisis de las Respuestas Respondida Respuestas Volver a las Pruebas Respuesta correcta (opcional). Delimita los blancos con doble guiones bajos __ Llene los Blancos Calificar la Prueba Marcar las respuestas correctas con un asterisco *. Nueva Pregunta Nueva Categoría de Preguntas Nueva Prueba No hay Tareas disponibles. Por favor agregue una nueva Tarea: %s Pregunta Categoría de Preguntas Categoría de Preguntas Preguntas Quiz Premium Prueba entregada. Pruebas Unas pruebas ya contienen esta pregunta. Por favor agregue una nueva prueba: %s Orden de Preguntas Aleatorio Selección de Opción Simple Mostrar las Respuestas Correctas No es más posible entregar esta prueba. Texto con blancos El cielo es __azul__.
El pasto es __verde__. Este programa está disponible en el módulo %s. Esta Prueba ya ha sido calificada. 