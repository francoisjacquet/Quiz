Módulo Quiz Premium
===================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es.png)

CARACTERÍSTICAS
---------------

- Copiar Prueba del Año Pasado
- Búsqueda de Preguntas
- Análisis de las Respuestas:
  - Identifique las preguntas para las cuales los estudiantes tienen dificultades
- Configuración:
  - Los Docentes solo pueden editar sus propias Preguntas y los Administradores u otros Docentes solo pueden verlas

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_es_3.png)

### [➭ Cambiar a Premium](https://www.rosariosis.org/es/modules/quiz/#premium-module)
