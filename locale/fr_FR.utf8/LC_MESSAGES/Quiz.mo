��    "      ,  /   <      �  E   �     ?     K     X     _     p     y     �     �  '   �     �  
   �  '   �               3  =   <     z     �     �  	   �     �     �     �  @   �          /     G  %   \     �  ,   �  +   �  "   �  �    I   �  
   A     L     `     i     �  	   �     �     �  +   �     �     	  3   	     C	      U	     v	  ?   �	     �	     �	     �	  	   �	     
     
     #
  P   (
     y
     �
      �
  /   �
       ,     .   =     l                                      "      !             	                                                                 
                                    *Correct option #1
Wrong option #2
Wrong option #3
*Correct option #4 *True
False Add Question Answer Answer Breakdown Answered Answers Back to Quizzes Correct answer (optional). Delimit gaps with double underscores __ Gap Fill Grade Quiz Mark correct answer with an asterisk *. New Question New Question Category New Quiz No Assignments are available. Please add a new Assignment: %s Question Question Categories Question Category Questions Quiz Premium Quiz submitted. Quizzes Quizzes already contain this question. Please add a new Quiz: %s Random Question Order Select One from Options Show Correct Answers Submissions for this quiz are closed. Text with gaps The sky is __blue__.
The grass is __green__. This program is available in the %s module. You already have graded this Quiz. Project-Id-Version: Quiz module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-01-07 18:58+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 *Choix correct #1
Choix incorrect #2
Choix incorrect #3
*Choix correct #4 *Vrai
Faux Ajouter la question Réponse Répartition des réponses Répondu Réponses Retour aux quiz Réponse correcte (optionnel). Délimiter les trous par deux tirets bas __ Texte à trous Noter le quiz Marquer les réponse correctes d'une astérisque *. Nouvelle question Nouvelle catégorie de questions Nouveau quiz Aucun devoir disponible. Merci d'ajouter un nouveau devoir : %s Question Catégories de questions Catégorie de questions Questions Quiz Premium Quiz répondu. Quiz Les quiz contiennent déjà cette question. Merci d'ajouter un nouveau quiz : %s Ordre des questions aléatoire Options à choix simple Afficher les réponses correctes Il n'est plus possible de répondre à ce quiz. Texte à trous Le ciel est __bleu__.
L'herbe est __verte__. Ce programme est disponible dans le module %s. Vous avez déjà noté ce quiz. 