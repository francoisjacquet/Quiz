Module Quiz Premium
===================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr.png)

CARACTÉRISTIQUES
----------------

- Copier un Quiz de l’année dernière
- Rechercher des questions
- Répartition des réponses :
  - Identifiez les questions pour lesquelles les élèves ont des difficultés
- Configuration :
  - Les enseignants ne peuvent éditer que leurs propres questions et les administrateurs ou autres enseignants peuvent seulement les consulter

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr_2-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr_2.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr_3-300.png)](https://www.rosariosis.org/wp-content/uploads/2018/12/quiz_premium_screenshot_fr_3.png)

### [➭ Passer à Premium](https://www.rosariosis.org/fr/modules/quiz/#premium-module)
