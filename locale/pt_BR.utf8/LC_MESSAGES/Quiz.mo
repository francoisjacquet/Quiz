��             +         �  E   �               (     /     8     @     P  '   k     �  
   �  '   �     �     �     �  =   �     9     B     V  	   h     r     w     �  @   �     �     �     �  %        9  ,   H  "   u  �  �  W   �     �     �  	          	        $     >  6   [     �     �  -   �     �     	     	  ;   2	     n	     w	     �	  	   �	     �	     �	     �	  O   �	     3
     R
     m
  :   �
     �
  ,   �
  &                                                              	                             
                                                        *Correct option #1
Wrong option #2
Wrong option #3
*Correct option #4 *True
False Add Question Answer Answered Answers Back to Quizzes Correct answer (optional). Delimit gaps with double underscores __ Gap Fill Grade Quiz Mark correct answer with an asterisk *. New Question New Question Category New Quiz No Assignments are available. Please add a new Assignment: %s Question Question Categories Question Category Questions Quiz Quiz submitted. Quizzes Quizzes already contain this question. Please add a new Quiz: %s Random Question Order Select One from Options Show Correct Answers Submissions for this quiz are closed. Text with gaps The sky is __blue__.
The grass is __green__. You already have graded this Quiz. Project-Id-Version: Quiz module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:25+0200
Last-Translator: Emerson Barros
Language-Team: RosarioSIS <info@rosariosis.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 *Opção correta nº 1
Opção errada nº 2
Opção errada nº 3
*Opção correta nº 4 *Verdadeiro
Falso Adicionar pergunta Responder Respondidas Respostas Voltar aos Questionários Resposta correta (opcional). Delimitar espaços em branco com sublinhados duplos __ Preencha os espaços em branco Notas do questionário Marque a resposta correta com um asterisco *. Nova pergunta Nova categoria de pergunta Novo questionário Não há tarefas disponíveis. Adicione uma nova tarefa: %s Pergunta Categorias de perguntas Categoria da pergunta Perguntas Questionário Questionário enviado. Questionários Os questionários já contêm esta pergunta. Adicione um novo questionário: %s Ordem de perguntas aleatórias Selecione uma das opções Mostrar respostas corretas As inscrições para este questionário estão encerradas. Texto com espaços em branco O céu está __azul__.
A grama é __verde__. Você já corrigiu este questionário. 