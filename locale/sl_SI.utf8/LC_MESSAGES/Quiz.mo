��            )         �  E   �     �                          (     8  '   S     {  
   �  '   �     �     �     �  =   �     !     *     >  	   P     Z     j  @   r     �     �     �  %   �       ,   +  "   X  �  {  a   e     �     �     �     �     �            )   5     _  
   q  &   |     �     �     �  5   �  
   	     	  
   ,	  
   7	     B	     O	  >   U	     �	     �	     �	  !   �	     
  %   
     :
            	                                                                                        
                                    *Correct option #1
Wrong option #2
Wrong option #3
*Correct option #4 *True
False Add Question Answer Answered Answers Back to Quizzes Correct answer (optional). Delimit gaps with double underscores __ Gap Fill Grade Quiz Mark correct answer with an asterisk *. New Question New Question Category New Quiz No Assignments are available. Please add a new Assignment: %s Question Question Categories Question Category Questions Quiz submitted. Quizzes Quizzes already contain this question. Please add a new Quiz: %s Random Question Order Select One from Options Show Correct Answers Submissions for this quiz are closed. Text with gaps The sky is __blue__.
The grass is __green__. You already have graded this Quiz. Project-Id-Version: Quiz module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:25+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 *Pravilna možnost #1
Napačna možnost št. 2
Napačna možnost št. 3
*Pravilna možnost št. 4 *Pravilno
Napačno Dodaj Vprašanje Odgovor Odgovorjeno Odgovori Nazaj na kvize Pravilen odgovor (neobvezno). Vrzeli razmejite z dvojnimi podčrtaji __ Zapolnitev vrzeli Oceni Kviz Pravilen odgovor označi z zvezdico *. Novo Vprašanje Nova Kategorija Vprašanj Nov Kviz Na voljo ni nobena dodelitev. Dodajte novo nalogo: %s Vprašanje Kategorije Vprašanj Kategorija Vprašanja Kviz poslan. Kvizi Kvizi že vsebujejo to vprašanje. Dodajte novo vprašanje: %s Naključni vrstni red vprašanj Izberite opcijo Prikaži pravilne odgovore Oddaje za ta kviz so zaključene. Besedilo z vrzelmi Nebo je modro__.
Trava je __zelena__. Ta kviz ste že ocenili. 